# Composants

## Champ texte

<div class="alert alert-info">
  Ce composant permet de laisser un texte <b>court</b> libre.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant

* **Valeur par défaut** : le texte qui sera affiché dans le composant. Il faudra alors que le/la répondant⋅e supprimer ce texte pour y mettre le sien

* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Préfixe** : texte situé **devant** et sur la même ligne que le composant

* **Suffixe** : texte situé **après** et sur la même ligne que le composant

* **Suggestion** : texte affiché **dans** le composant et qui le sera jusqu'à ce que le/la répondat⋅e saisisse son texte (incompatible avec **Valeur par défaut** - vous devez mettre l'un ou l'autre)

* **Taille** : taille du composant (par exemple : 20%, 50% etc…)

* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

### Validation

* **Longueur maximale** : longueur du texte (Si vous indiquez `3`, il ne sera pas possible de mettre plus de 3 caractères dans le champ)

## Courriel

<div class="alert alert-info">
  Ce composant permet de récupérer une adresse mail (avec test de validité).
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant

* **Valeur par défaut** : le texte qui sera affiché dans le composant. Il faudra alors que le/la répondant⋅e supprimer ce texte pour y mettre le sien

* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Suggestion** : texte affiché **dans** le composant et qui le sera jusqu'à ce que le/la répondat⋅e saisisse son texte (incompatible avec **Valeur par défaut** - vous devez mettre l'un ou l'autre)

* **Taille** : taille du composant (par exemple : 20%, 50% etc…)

* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

## Boutons radios

<div class="alert alert-info">
  Ce composant permet de poser une question à choix multiples avec <b>une seule réponse acceptée</b>.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant

* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

### Options

Valeurs à afficher.

![composants_forms_boutons_radios_options image](images/composants_forms_boutons_radios_options.png)

## Liste de sélection

<div class="alert alert-info">
  Ce composant permet de poser une question à choix multiples avec <b>une seule réponse acceptée</b> dans <b>une liste déroulante</b>.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant

* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

### Options

Valeurs à afficher.

![composants_forms_liste_selection_options image](images/composants_forms_liste_selection_options.png)


## Heure

<div class="alert alert-info">
  Ce composant permet de sélectionner une heure.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **valeur par défaut** : à utiliser avec la nomenclature anglo-saxonne :
  * `now` pour maintenant
  * `1:37pm` pour 13h37
  * `1:37am` pour 01h37
* **Fuseau horaire par défaut** :
  * **Fuseau horaire de l'utilisateur** : le fuseau du navigateur du répondant ou de la répondante
  * **Fuseau horaire du site** : heure française (UTC+1)
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

![composants_forms_heure_proprietes image](images/composants_forms_heure_proprietes.png)

### Affichage

* **Format du temps** :
  * **12 heures (am/pm)** pour un affichage sur 12 heures (`1:37pm`)
  * **24 heures** pour un affichage sur 24 heures (`13h37`)
* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

## Caché


<div class="alert alert-info">
  Ce composant permet d'ajouter du texte qui ne sera visible qu'en cas de changement de page ou dans les courriels.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **valeur par défaut** : ce qui sera affiché (dans la page suivante ou dans le courriel)

## Groupe de champs

<div class="alert alert-info">
  Ce composant permet de créer des groupes de composants.
</div>

![composants_forms_groupe image](images/composants_forms_groupe.png)

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant.
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Repliable** : permet de replier/déplier un groupe
* **Replié** : le groupe sera replié par défaut

## Zone de texte

<div class="alert alert-info">
   Ce composant permet de laisser un texte <b>long</b> libre.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **Valeur par défaut** : le texte qui sera affiché dans le composant. Il faudra alors que le/la répondant⋅e supprimer ce texte pour y mettre le sien
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Suggestion** : texte affiché **dans** le composant et qui le sera jusqu'à ce que le/la répondat⋅e saisisse son texte (incompatible avec **Valeur par défaut** - vous devez mettre l'un ou l'autre)
* **Rangées** : nombre de rangées par défaut (lignes)
* **Colonnes** : largeur de la zone de texte
* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

## Nombre

<div class="alert alert-info">
  Ce composant permet de saisir <b>uniquement</b> des nombres.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **Valeur par défaut** : le texte qui sera affiché dans le composant. Il faudra alors que le/la répondant⋅e supprimer ce texte pour y mettre le sien
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Préfixe** : texte situé **devant** et sur la même ligne que le composant
* **Suffixe** : texte situé **après** et sur la même ligne que le composant
* **Position après la virgule** : nombre de décimales (la réponse `12` deviendra `12.0000000` si `7` est paramétré)
* **Séparateur de milliers** : **Virgule** (`1,000`), **Point** (`1.000`), **espace** (`1 000`) ou **Aucun** (`1000`)
* **Marqueur décimal** : **Point** (`12.00`) ou **Virgule** (`12,00`)
* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)


## Cases à cocher

<div class="alert alert-info">
   Ce composant permet de poser une question à choix multiples avec <b>plusieurs réponses acceptées</b>.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Affichage

* **Afficher l'étiquette** : affiche le titre **au-dessus**, **sur la même ligne** que le composant ou pas du tout (**Aucun(e)**)

## Grille

<div class="alert alert-info">
   Ce composant permet de créer un tableau avec plusieurs lignes et choix. <b>Une seule réponse possible par ligne</b>.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

## Date

<div class="alert alert-info">
  Ce composant permet de saisir une date.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **Valeur par défaut** : la date qui sera affichée dans le composant (au format anglo-saxon) :
  * `2018/3/14` pour le 14 Mars 2018
  * `now` pour la date actuelle
  * `tomorrow` pour le lendemain
  * `next week` pour la semaine prochaine
  * etc
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

## Fichier

<div class="alert alert-info">
   Ce composant permet de joindre un fichier au questionnaire.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner au composant
* **Destination pour le transfert** : lorsque l'option `Fichiers privés` est choisie, deux fichiers identiques seront sauvegardés deux fois alors qu'avec `Fichier public`, il n'y aura qu'un seul exemplaire (ce qui prendra moins de place sur le serveur)
* **Description** : le texte qui sera situé sous le composant (permettant d'ajouter un complément d'information)

### Validation

* **Extensions de fichiers autorisées** : ![Extensions fichiers autorisées image](images/composants_forms_fichier_extensions.png)

En cliquant sur *sélectionner*, vous pouvez sélectionner toutes les extensions de la catégorie.

## Balisage

<div class="alert alert-info">
   Ce composant permet d'ajouter du texte entre les composants. Peut être utile pour des explications.
</div>

* **Titre** : le nom que vous souhaitez donner au composant
* **Balisage** : le texte qui sera affiché. Supporte le HTML.

## Saut de page

<div class="alert alert-info">
   Ce composant permet d'afficher son questionnaire sur plusieurs pages.
   <b>ATTENTION</b> : <b>Afficher la barre de progression</b> et <b>Afficher le titre de la page depuis le composant "saut de page" </b> doivent être cochés dans <b>Formulaire</b> > <b>Paramètres avancés du formulaire</b> > <b>Barre de progression</b>.
</div>

### Propriétés

* **Titre** : le nom que vous souhaitez donner à la page
