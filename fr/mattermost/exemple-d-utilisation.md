# Exemple d’utilisation de Framateam

## L’asso LICORNES veut quitter son groupe Facebook

Vous ne connaissez pas la **Ligue des Infatigables Comparses Optimistes
Reniant le Nihilisme et Éclatants de Sollicitude**…? Si, en réalité, cette
association n’existe pas&hellip; [elle devrait](http://yakafokon.detected.fr/)&nbsp;! Sandrine, la présidente, en a
marre d’utiliser Facebook pour discuter avec les membres, sans compter
le Skype ouvert en permanence à côté pour *chatter* en privé avec le
Conseil d’Administration ou le bureau de l’asso.

### Créer sa team

Elle décide donc de se créer un compte
[Framateam](https://framateam.org). Ça, c’est facile : le truc
classique, en trois étapes :

1.  Créer sa team (chouette : c’est elle qui décide si la team entre
    dans l’annuaire public ou non&nbsp;!)
2.  Se créer un compte (elle, elle utilise son compte Gitlab chez
    [Framagit](https://framagit.org), parce que c’est une pro du Perl)
3.  Inviter les membres de l’asso avec leur email (elle teste avec
    l’email de Gérard, elle invitera les autres quand ce sera prêt)

Très vite, elle se rend compte que [Framateam](https://framateam.org)
marche sous forme de canaux de discussion : il y a déjà le **Centre
Ville**, pour la vie de l’asso, et le **Hors Sujet**, pour les
galéjades. Ça tombe bien, chez les LICORNES, ça *galèje* souvent. Elle
décide de créer en plus un canal pour son équipe de graphistes tout
terrain, qui font des affiches à paillettes et des sites web mirifiques.

![framateam nouveau canal zoom](images/framateam-nouveau-canal-zoom.png)

### Premiers échanges

Et voilà que pendant qu’elle mitonnait ses canaux de discussion dans son
coin, Gérard est déjà arrivé sur leur [Framateam](https://framateam.org)
et y poste le lien vers une image de licorne musclée qu’il a trouvée…
Magie de Mattermost : l’image s’affiche automatiquement&nbsp;! Sandrine
répond — forcément — avec un chaton-licorne (mieux connu sous le nom de
"Dieu des Zinternetz"). 

![Framateam images](images/Framateam-images.png)

### Création de canaux

![Framateam canaux](images/Framateam-canaux.png)

Faut dire que pendant ce temps, Sandrine a eu le temps de créer
plusieurs canaux de conversations.

-   Des publics (ouverts à tout membre de la team) :
    -   Le **Centre Ville** et le **Hors Sujet**, qu’elle a décidé de
        garder
    -   Le canal pour les **Graphistes** tout terrain est prêt.
    -   Il en fallait un pour les **Événements** de l’asso (les soirées
        Paillettes et autres rencontres Arc-En-Ciel : c’est de l’orga&nbsp;!)
    -   Pour la **Trésorerie** (laissons-les parler sous de leur côté,
        se dit-elle…)


-   Mais aussi des groupes privés (où il faut sélectionner les membres
    de la team qui y participeront) :
    -   Un pour le **Conseil d’Administration**
    -   Un pour le **Bureau**
    -   Un pour préparer l’**anniversaire de Gérard** dans son dos&nbsp;;)

### Mise en forme des messages

D’ailleurs, pendant que Gérard s’amuse à inviter les autres membres du
groupe sur [Framateam](https://framateam.org) (en leur envoyant un
simple [lien d’invitation à l’équipe](https://framateam.org/signup_user_complete/?id=os6tyf46ub88dynpm5kkbhfh9y)
!), elle décide de préparer le message pour organiser la
*surprise-party* de l’anniversaire de son comparse :
![Framateam canal secret](images/Framateam-canal-secret.png)

Alors comment a-t-elle fait pour mettre en page un aussi joli message&nbsp;?
Sandrine avait tout simplement cliqué sur "aide" en bas à droite et a
lu, dans la documentation (traduite avec brio par le groupe
[Framalang](https://participer.framasoft.org/traduction-rejoignez-framalang/)),
qu’il suffisait d’écrire son message en
[Markdown](https://fr.wikipedia.org/wiki/Markdown) (LA syntaxe facile à
retenir et utiliser). D’ailleurs elle a fait une coquille sur son
message, elle clique donc sur le `[&hellip;]` à droite de son message pour le
modifier : 
![Framateam markdown zoom](images/Framateam-markdown-zoom.png)

### Fil de discussion et recherche

De retour sur la discussion principale, Sandrine se rend compte que sa
question à Gérard (« Mais où sont passés nos flyers&nbsp;? ») s’est un petit
peu perdue dans les échanges. Néanmoins Gégé a eu la bonne idée de
répondre directement à sa demande en utilisant la flèche à droite de son
message.
![Framateam conversation 2](images/Framateam-conversation-2-1.png)

Car oui : le logiciel Mattermost qui fait tourner Framateam permet de
conserver tous les messages et de faire des recherches dans les
discussions. Quelques jours plus tard, Sandrine fait une simple
recherche du mot "flyer", ce qui lui permet de retrouver son message
ainsi la réponse de Gérard. Elle le relance donc :
![Framateam conversation](images/Framateam-conversation-2.png)


### Notifications

Sandrine connaît son Gégé-accros-aux-emails : elle a donc mis une
arobase devant son pseudo :

![Framateam conversation zoom 2](images/Framateam-conversation-zoom-2.png)

Gérard n’était pas devant son écran, il a reçu un joli email de Framateam pour
lui signaler qu’il a été mentionné dans une conversation.
![Framateam email notification](images/Framateam-email-notification.png)


## La morale de cette histoire…?

C’est que les flyers étaient bien dans le coffre de la voiture de
Gérard. &hellip; C’est surtout que les LICORNES se sont un peu plus libérées
de Facebook, et peuvent désormais organiser leurs distributions de
paillettes sans craindre de nourrir de leurs *data* l’ogre bleu de
Zuckerberg. Et même si vous croyez que les LICORNES n’existent pas (à
vous de les créer comme on l’a fait pour le Framablog ^^),
[Framateam](https://framateam.org) existe bel et bien. **À vous d’y
créer votre (ou vos) équipe(s) sur [Framateam.org](https://framateam.org)&nbsp;!**  
