Guide d'utilisation de Mastodon
=====================


## Premiers pas

#### Paramétrer votre profil

Vous pouvez personnaliser votre profil Mastodon de plusieurs manières : vous pouvez choisir un pseudo personnalisé visible par les autres utilisateurs, un avatar pour votre profil, une image d'arrière-plan pour votre en-tête de page de profil, et une courte « bio » qui présente votre compte ou vous-même.

![icône Préférences](screenshots/preferences.png) Pour modifier votre profil, cliquez sur l'icône Préférences dans la colonne Composer et sélectionnez « Modifier Profil » sur le menu de gauche dans la page Préférences. Votre pseudo est limité à 30 caractères, votre bio à 160. Les avatars et images d'en-tête peuvent être mis en ligne aux formats png, gif ou jpg et ne peuvent pas dépasser 2 Mo. Elles seront modifiées à la taille standard - 120 x 120 pixels pour les avatars, 700 x 335 pixels pour les images d'en-tête.

#### Notifications e-mail

![icône Préférences](screenshots/preferences.png) Mastodon peut vous avertir d'une activité par courriel si vous le souhaitez. Pour régler vos paramètres afin de recevoir des notifications par courriel, cliquez sur l'icône Préférences dans la colonne Composer et sélectionnez la page « Préférences » depuis le menu de gauche. Vous y trouverez un certain nombre de cases pour autoriser ou non les notifications par courriel selon plusieurs types d'activités.

#### Publier un message

La fonctionnalité la plus simple de Mastodon consiste à créer un message, aussi appelé un *pouet*. Pour publier un *pouet*, il suffit de saisir le message que vous voulez poster dans le champ de texte « Exprimez-vous » dans la colonne Composer et de cliquer sur « pouet ». Un pouet est limité à 500 caractères : si vous avez vraiment besoin de davantage de caractères, vous pouvez répondre à vos propres pouets afin qu'ils s'affichent comme une conversation.

Si vous voulez répondre au pouet d'un autre utilisateur, cliquez sur l'icône « Répondre » de ce pouet. Cela va inclure son pseudo directement dans la fenêtre d'édition ainsi qu'un aperçu du message auquel vous répondez. L'utilisateur recevra une notification de votre réponse.

De la même manière, pour débuter une conversation avec un autre utilisateur, il suffit de mentionner son pseudo dans votre pouet. En saisissant le symbole @ suivi directement (sans espace) de n'importe quel caractère, Mastondon va automatiquement suggérer des utilisateurs dont le pseudo correspond à votre saisie.

Comme pour les réponses, mentionner un utilisateur va lui envoyer une notification. Si le message commence par une mention, il sera traité comme une réponse et n'apparaîtra que dans les historiques des utilisateurs qui suivent à la fois vous-même et l'utilisateur que vous mentionnez. Il sera visible dans votre profil selon les paramètres de confidentialité.


##### Avertissement sur le contenu

Lorsque vous souhaitez poster quelque chose qui ne doit pas être immédiatement visible  —  par exemple, des spoilers d'un film qui vient tout juste de sortir, ou des réflexions personnelles sur un sujet qui pourrait être polémique — vous pouvez le « cacher » derrière un avertissement de contenu.

Pour cela, cliquez sur le bouton « Avertissement » de la boîte de composition. ![icône Avertissement](screenshots/compose-cw.png) Cela ajoutera une nouvelle boîte texte marquée « Avertissement » : c'est à cet endroit qu'il faut entrer un court résumé de ce que contient le « corps » de votre message, tandis que la publication en elle-même doit être entrée dans la boîte « Exprimez-vous », comme d'habitude.

![animation montrant comment mettre en place un avertissement](screenshots/content-warning.gif)

Le corps de votre publication sera masqué derrière un bouton « Voir Plus » dans l’historique et seuls seront visibles par défaut l'avertissement ainsi que tout utilisateur mentionné


![animation montrant un avertissement de contenu dans la timeline](screenshots/cw-toot.gif)

**NOTEZ** que cela ne cachera pas les images contenues dans votre message  —  les images peuvent être marquées comme « sensibles » d'autre part afin de les masquer tant qu'on n'a pas cliqué sur elles. Pour en savoir plus sur cette fonctionnalité, voir la section [Poster des images](User-guide.html#poster-des-images) de ce guide.

##### Hashtags

Si vous créez un message dans le cadre d'un sujet plus large, il peut être utile de lui ajouter  un *hashtag*. Cela se fait simplement en ajoutant un signe # suivi d'un texte, p. ex. #présentation (qui est populaire sur mastodon.social pour les nouveaux utilisateurs qui désirent se présenter à la communauté), ou #politique pour les discussions politiques, etc.

Le fait de cliquer sur un hashtag dans un pouet affiche un historique constitué des messages publics qui contiennent ce hashtag (c'est en fait un raccourci pour effectuer une recherche sur lui). Ceci permet aux utilisateurs de regrouper les messages traitant de sujets similaires, en formant un historique séparé pour les gens intéressés par un sujet donné. Il est également possible d'effectuer une recherche sur un hashtag depuis la barre de recherche au-dessus de la boîte de composition.

##### Partages et favoris

Vous pouvez *marquer en favori* le pouet d'un autre utilisateur en cliquant sur l'étoile en dessous. Cela notifiera à l'utilisateur que vous avez marqué son message en favori ; la signification de ce genre de marquage varie fortement avec le contexte, depuis une simple indication « j'écoute », jusqu'à l'expression d'une approbation ou une offre de soutien aux idées exprimées.

En outre, vous pouvez *partager* des pouets en cliquant sur les flèches circulaires. Partager un pouet l'affiche sur l'historique de votre profil et le rend visible à tous vos suiveurs, même s’ils ne suivent pas l'auteur du message d'origine. C'est utile lorsque vous pensez qu'un message posté par un utilisateur mérite d'être vu par d'autres, car cela étend la portée de ce message tout en conservant intacte l'information originale.


#### Poster des images

![icône image](screenshots/compose-media.png) Pour publier une image, cliquez ou appuyez tout simplement sur l’icône image dans la colonne Composer et sélectionnez le fichier à envoyer en ligne.
Vous pouvez également glisser-déposer une image depuis le bureua de votre ordinateur vers la zone de saisie du Pouet :

![glisser-déposer d'une image](screenshots/dragNdropImage.gif)

Si l'image est *not safe for work* (« ne pas utiliser au travail »), donc destinée à un usage personnel) ou contient un contenu choquant, vous pouvez sélectionner le ![bouton NSFW](screenshots/compose-nsfw.png) « Activer NSFW » qui apparaît une fois que vous avez ajouté une image. Cela masquera par défaut l'image de votre message, le rendant cliquable pour afficher la prévisualisation. Il s'agit de la version visuelle des [avertissements de contenu](User-guide.html#avertissement-sur-le-contenu) avec lesquels elle peut être combinée, s’il existe du texte qui accompagne l'image  —  sinon cela suffit à marquer l'image comme sensible et à  faire du corps de votre message un avertissement de contenu.

Vous pouvez aussi joindre des fichiers vidéo ou des animations GIF dans vos pouets. Cependant, ceux-ci ne peuvent pas excéder la taille limite de 4 Mb et les vidéos doivent être dans le format .webm ou .mp4.

#### Suivre d'autres utilisateurs

Suivre d'autres utilisateurs fait apparaître tous leurs pouets ainsi que les pouets que d'autres utilisateurs auront [partagés](User-guide.html#partages-et-favoris) dans votre colonne d'accueil. Cela vous permet d'avoir un fil d'actualités, distinct des [fils publics](User-guide.html#fil-d'actualités-public), dans lequel vous pouvez lire ce que certaines personnes disent sans le bruit de la conversation générale.

![Icône Suivre](screenshots/follow-icon.png)  Pour suivre un utilisateur, cliquer sur son nom ou son avatar pour ouvrir son profil, puis cliquer sur l'icône *Suivre* en haut à gauche de la vue de son profil.

Si son compte présente une icône de cadenas ![Padlock icon](screenshots/locked-icon.png) à côté de son pseudo, il reçoit une notification de votre demande de le suivre et doit l'approuver avant que vous ne soyez ajouté à la liste de ses suiveurs (et par conséquent capable de voir ses pouets). Pour vous indiquer que vous êtes en attente d'approbation d'une requête de suivi, l'icône *Suivre*  ![Follow icon](screenshots/follow-icon.png) de leur profil, est remplacée par l'icône d'un sablier  ![Pending icon](screenshots/pending-icon.png). Les critères d'approbation des nouveaux suiveurs peuvent être activés pour votre propre profil dans la rubrique *Préférences*.

Une fois que vous suivez un utilisateur, l'icône *Suivre* est surlignée en bleu dans son profil ![Following icon](screenshots/following-icon.png) ; vous pouvez annuler son suivi en re-cliquant dessus.

Si vous connaissez le pseudo de quelqu'un, vous pouvez aussi ouvrir son profil pour le suivre en le saisissant dans le [champ de recherche](User-guide.html#faire-une-recherche) de la colonne *Compose*. Cela fonctionne aussi pour des utilisateurs distants, mais suivant s’ils sont connus ou non de votre instance locale, il vous faudra peut-être saisir leur nom complet y compris avec le domaine (p. ex. `gargron@mastodon.social`) dans le champ de recherche avant que leur profil n'apparaisse dans les suggestions.

Autre possibilité : si vous disposez déjà du profil d'un utilisateur ouvert dans un onglet séparé du navigateur, tous les réseaux en relation avec OStatus devraient avoir un bouton *Suivre* ou *S'abonner* dans leur page de profil. Cela vous demande d'entrer le nom complet de l'utilisateur à suivre (c.-à-d. que si votre compte est sur mastodon.social, il vous faut saisir `myaccount@mastodon.social`).

#### Notifications

Vous recevez une notification à chaque fois que quelqu'un suit votre compte ou demande à vous suivre, mentionne votre pseudo, ou met en favoris un vos pouets. Elle apparaîtra comme une notification sur votre ordinateur (si votre navigateur internet le permet et que vous avez activé cette option) ainsi que dans votre colonne « Notifications ».

![Icône de Paramétrage des Notifications](screenshots/notifications-settings.png) Vous pouvez filtrer le type de notifications que vous voyez dans la colonne Notifications en cliquant sur l'icône Paramétrage des Notifications en haut de la colonne, puis en cochant ou dé-cochant les notifications que vous voulez voir ou non.

![Icône Nettoyer](screenshots/notifications-clear.png) Si vos notifications deviennent encombrantes, vous pouvez nettoyer la colonne en cliquant sur l'icône Nettoyer en haut de la colonne, cela effacera son contenu.

![Icône Préférences](screenshots/preferences.png) Vous pouvez aussi les notifications de personnes que vous ne suivez pas ou qui ne vous suivent pas totalement  — pour cela, cliquez sur l'icône Préférences dans la colonne Composer, sélectionnez « Préférences » dans le menu de gauche et vérifiez les options respectives « Bloquer les Notifications ».

#### Applis mobiles

Mastodon possède une API ouverte afin que tout le monde puisse développer un client ou une application pour utiliser Mastodon depuis n'importe quel support. De nombreuses personnes ont déjà développé des applications pour iOS et Android. Vous trouverez une liste de ces applications [ici](Apps.html). De nombreux projets sont *open source* et accueillent volontiers des collaborateurs.

#### Fils d'actualités publics

En plus de votre fil d'actualité d'accueil, deux fils d'actualités publiques sont disponibles : le Fil public global et le fil d'actualité local. Ce sont tout deux de bons moyens de rencontrer de nouvelles personnes à suivre ou avec lesquelles interagir.

#### Le Fil public global

Le fil d'actualités fédéré montre à tout le monde les messages de tous les utilisateurs connus de votre instance. Cela signifie que n'importe quel utilisateur sur la même instance que vous, ou tout autre personne de votre instance, suit cet utilisateur. Le fil public global est un bon moyen pour participer aux discussions générales du monde. Les utilisateurs que vous suivez  présent sur d'autres instances que vous rencontrez sur le fil public global.

![Federated Timeline icon](screenshots/federated-timeline.png) Pour voir le fil public global, cliquez sur l'icône « Fil public local » dans votre colonne de composition ou sur le bouton qui lui correspond dans le panneau de démarrage. Pour masquer le fil global, il vous suffit de cliquer sur le bouton « Retour » en haut de la colonne que vous parcourez.


#### Le Fil public local

Le fil public local ne montre que les messages des utilisateurs de votre instance d'accueil. Cela peut être utile si votre instance a des normes de communauté particulières que les utilisateurs d'autres instances pourraient ne pas avoir, tels que des sujets particuliers soumis à des avertissements liés au contenu, des plaisanteries privées, ou des intérêts communs. Pour voir le fil public local, cliquez sur ![Menu icon](screenshots/compose-menu.png) l'icône de Menu dans le panneau Compose, puis sélectionnez « Fil public local » dans la colonne la plus à droite.

#### Faire une recherche

Mastodon a une fonction de recherche. Vous pouvez l'utiliser pour rechercher des utilisateurs et des [hashtags](User-guide.html#hashtags). La recherche n'est pas réalisée sur l'intégralité du texte de chaque message, uniquement sur les hashtags. Pour faire une recherche, vous n'avez qu'à écrire dans le champ de recherche dans la colonne Composer et appuyer sur *Entrée*, cela ouvrira le panneau de recherche. Celui-ci vous montrera des suggestions pendant la frappe. Si vous sélectionnez l'une d'entre elles, cela ouvrira le profil d'un utilisateur ou une vue de tous les pouets liés au hashtag.

## Confidentialité, sûreté et sécurité

Mastodon possède un certain nombre d'options de sécurité et confidentialité qui n'existent pas sur des réseaux plus publics comme Twitter. Les paramètres de vie privée, en particulier, sont plus détaillés ; leur fonctionnement est expliqué dans cette section.

#### Authentification à deux facteurs

L’authentification à deux facteurs (2FA) est un mécanisme qui améliore la sécurité de votre compte Mastodon en demandant un code numérique depuis un autre appareil (la plupart du temps un smartphone) lié à votre compte Mastodon quand vous vous connectez - cela signifie que même si quelqu'un parvient à obtenir votre adresse mail et votre mot de passe, il ne pourra pas pirater votre compte Mastodon puisqu'il aurait aussi besoin d'un appareil que vous possédez pour se connecter.

L'authentification à double facteur de Mastodon utilise Google Authenticator (ou des applications compatibles comme Authy). Vous pouvez l'installer gratuitement sur votre appareil sous [Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) ou [iOS](https://itunes.apple.com/gb/app/google-authenticator/id388497605). [Cette page Wikipédia](https://en.wikipedia.org/wiki/Google\\_Authenticator#Implementations) liste les autres versions de l'application disponibles pour d'autres systèmes.


![Preferences icon](screenshots/preferences.png) Afin d'activer 2FA pour votre compte Mastodon, cliquer sur l'icône *Préférences* dans la colonne de composition, cliquez sur « Authentification à deux facteurs » dans le menu de gauche de la page de configuration et suivez les instructions. Une fois activé, à chaque fois que vous vous connectez, vous avez besoin d'un code utilisable une seule fois généré par l'application d'authentification sur l'appareil que vous avez associé à votre compte.

#### Confidentialité du compte

Pour vous permettre de mieux contrôler qui peut voir vos pouets, Mastodon peut gérer des comptes « privés » ou « verrouillés ». Si votre compte est privé, vous recevrez une notification à chaque fois que quelqu'un veut vous suivre, et vous pouvez autoriser ou refuser la demande de suivi. De plus, si votre compte est privé, tous les nouveaux pouets que vous publiez seront « privés » par défaut (voir la section ci-dessous [Confidentialité des pouets](User-guide.html#confidentialité-des-pouets)).

![Icône des préférences](screenshots/preferences.png) Pour rendre votre compte privé, cliquez sur l'icône des préférences dans le panneau d'écriture, sélectionnez « Modifier le profil » et cochez la case « Rendre le compte privé », puis cliquez sur « Enregistrer les modifications ».

![Capture d'écran du paramètre « compte privé »](screenshots/private.png)

#### Confidentialité des pouets

La confidentialité des pouets est gérée indépendamment de celle des comptes, et individuellement pour chacun des pouets. Les quatre statuts de visibilité des pouets sont Public (par défaut), Non-listé, Privé, et Direct. Pour choisir votre niveau de confidentialité, cliquez sur l'icône de globe ![Globe icon](screenshots/compose-privacy.png). Les modifications de ces paramètres sont persistantes d'un message à l'autre, c'est-à-dire : si vous écrivez un pouet privé, chaque pouet que vous ferez ensuite sera également privé jusqu'à ce que vous décidiez de le passer en public. Vous pouvez modifier le paramétrage par défaut de la confidentialité en passant par les Préférences.


**Public** est le statut par défaut des pouets pour la plupart des comptes. Les pouets publics sont visibles par tous les autres utilisateurs sur leur fil d'actualité public, liés à d'autres instances de Mastodon et OStatus sans restriction, et apparaissent sur votre page de profil aux yeux de n'importe qui, y compris les moteurs de recherche et les visiteurs non-connectés à un compte Mastodon.

Les pouets **non-listés** sont publics, mais ils n’apparaissent pas dans le fil d'actualités public ou dans les résultats de recherches. Ils sont visibles par n'importe qui vous suivant et apparaissent sur votre page de profil au public, même sans un compte Mastodon. Excepté qu'ils n’apparaissent pas dans le fil d'actualités public ou dans les résultats de recherches, ils fonctionnent exactement comme les messages publics.

Les pouets **privés** ne sont pas visibles sur le fil d'actualités public, ni sur votre page de profil pour quiconque, excepté ceux qui sont dans votre liste de suiveurs. Cette option est de peu d'utilité si vous n'avez pas choisi d'approuver vos nouveaux suiveurs (n'importe qui pouvant alors vous suivre et donc voir vos pouets privés). Cependant, l'intérêt de cette séparation est que si vous *réglez* votre compte entier en privé, vous pouvez le désactiver sur un pouet pour le passer en non-listé ou même public.

Les pouets privés ne peuvent pas être mis en favoris. Si quelqu'un que vous suivez fait un pouet privé, il apparaîtra dans votre fil d'actualités avec une icône de cadenas à la place de l'icône Partager. **Remarquez** que les instances retirées ne respectent pas forcément ce principe.

Les pouets privés ne se fédèrent pas sur les autres instances, sauf en @mentionnant un utilisateur distant. Dans ce cas, ils sont fédérés avec leurs instances, et les utilisateurs sur cette instance qui vous suivent ainsi que la personne @mentionnée va voir le message dans son historique d’accueil. Il n’y a pas de façon sûre de vérifier si une instance va en réalité respecter la confidentialité du message. Les serveurs non-Mastodon, comme les serveurs GNU Social, ne prennent pas en compte les paramètres de vie privée de Mastodon. Un utilisateur sur GNU Social qui vous @mentionne dans un message privé ne sera même pas conscient qu’il est destiné à être privé et sera capable de le partager, ce qui « cassera » les paramètres de vie privée. Il n’y a pas non plus aucune garantie que quelqu’un ne va pas modifier le code de son instance Mastodon pour ne pas respecter les restrictions des messages privés. Un avertissement sera affiché si vous écrivez un pouet privé qui sera fédéré avec une autre instance. Vous devrez donc faire attention à la confiance que vous accordez à l’utilisateur que vous @mentionnez et l’instance utilisée.

Les messages privés ne sont pas chiffrés. Il faut donc que vous soyez sûr⋅e que vous avez suffisamment dans la personne qui administre votre instance pour ne pas lire vos messages privés dans les coulisses. Il est recommandé de ne rien publier de ce que vous ne souhaitez pas voir intercepté.

les messages **Directs** sont visibles seulement par les utilisateurs que vous avez @mentionnés et qui ne peuvent pas être partagés. Comme pour les messages privés, vous devez avoir à l'esprit qu'une instance distante peut ne pas respecter ce protocole. Si la conversation que vous envisagez est sensible il est prudent de la mener hors de Mastodon.

Récapitulons :

Confidentialité des pouets| Visibilité sur le profil| Visibilité publique                |Fédéré avec autres instances
------------------------- | ----------------------- | ---------------------------------- | ---------------------------
Public                         |Tout le monde y compris visiteurs anonymes| Oui          | Oui
Non-listé                      |Tout le monde y compris visiteurs anonymes| Non          | Oui
Privé                     | Suiveurs seulement      | Non                 |  @mentions distantes seulement
Direct                    | Non| Non                | @mentions distantes seulement

#### Bloquer

Vous pouvez bloquer des utilisateurs dans le but de les empêcher de vous contacter. Pour ce faire, vous pouvez cliquer sur l'icône du Menu sur l'un de leurs pouets ou sur leur profil, et sélectionner « Bloquer ».

.
**Note** Cela les empêchera de voir vos pouets publics tant qu'ils sont connectés, mais ils pourront tout de même les voir en ouvrant votre profil dans un autre navigateur sur lequel ils ne sont pas connectés à Mastodon (ou bien s'ils sont connectés avec un compte différent que vous n'avez pas bloqué).

Les mentions, favoris, partages ou tout autre interaction avec vous provenant d'un utilisateur bloqué vous seront cachés. Vous ne verrez pas les réponses à une personne bloquée, pas même si la réponse vous mentionne, et vous ne verrez pas non plus leurs pouets si quelqu'un les partage.

Les utilisateurs bloqués ne seront pas notifiés de votre blocage. Ils seront enlevés de vos suiveurs.

#### Mettre en sourdine

Si vous ne souhaitez pas voir les messages d'un utilisateur particulier, mais ne vous préoccupez pas qu'il puisse voir les vôtres, vous pouvez choisir de les *ignorer*. Vous pouvez ignorer un utilisateur depuis le même menu sur le profil où vous pouviez les bloquer. Vous ne verrez pas les messages d'un utilisateur ignoré, sauf s’il vous @mentionne. Un utilisateur ignoré n'aura pas de moyen de savoir que vous l'avez ignoré.


#### Signaler des pouets ou des utilisateurs

Si vous rencontrez un pouet ou un utilisateur qui ne respecte pas les règles de votre instance ou que voulez signaler à l'administrateur de l'instance (par exemple si une personne harcèle un autre utilisateur, *spamme* de la pornographie ou poste du contenu illégal), vous pouvez cliquer sur le bouton menu « … » du pouet ou sur le menu « hamburger » sur le profil et choisir de le signaler. La colonne la plus à droite va alors afficher le formulaire suivant :

![Formulaire de signalement](screenshots/report.png)


Dans ce formulaire, vous pouvez sélectionner n'importe quel pouet que vous voudriez signaler à l'administrateur de l'instance, et ajouter un commentaire pour aider à identifier ou résoudre le problème (de « est un spammeur » à « ce pouet contient de la pornographie non annoncée »). Le signalement va être visible sur le serveur administrateur une fois qu'il a été envoyé afin que soit choisie la réaction appropriée, par exemple masquer les pouets de l'utilisateur du fil d'actualités public ou bannir le compte.

#### Crédits
Ce guide destiné aux utilisateurs est une traduction de la version anglaise qui est évolutive. Il est donc susceptible de modifications.
La version française a été assurée par l'équipe Framalang : Félicien, Bidouille, QS (Marie), mo, egilli, didimo, Mika, simon, egilli, Asta, Docendo, Lumi, Pouhiou, xi, Penguin, jaaf, Opsylac, goofy, gyom, lanodan, Bromind + 2 anonymes

version 0.9 11/04/2017
