# Framavox illustré en un exemple concret
---------------------------------------

## Le *Groupe d’Action pour le Gras* a des décisions à prendre&hellip;

Sandrine (dont la vie associative [est bien
remplie](https://framablog.org/2016/05/10/framateam-liberez-vos-equipes-des-groupes-facebook-et-de-slack/)&hellip;)
est une membre du G.A.G., le «&nbsp;Groupe d’Action pour le Gras&nbsp;» dont la
devise est «&nbsp;Le gras, c’est la vie.&nbsp;» Les membres de cette association
ont du mal à se réunir physiquement pour prendre toutes les décisions
nécessaires. Elle décide donc de créer un groupe pour l’association sur
[Framavox](https://framavox.org)&hellip; C’est simple : elle se crée un
compte, puis elle s’y connecte pour créer le groupe.
![01 framavox creer groupe](images/01-framavox-creer-groupe.png)

Sandrine décide de créer un groupe fermé, car elle voit dans la
documentation de Loomio (traduite en français par l’infatigable groupe
[Framalang](https://participer.framasoft.org/traduction-rejoignez-framalang/))
qu’elle pourra modifier ce paramètre ultérieurement. Dès la création,
une première discussion de «&nbsp;bienvenue&nbsp;» est créée. L’œil de lynx de
Sandrine y repère bien vite :

1.  La zone d’accès rapides aux discussions récentes / non lues et à ses
    groupes
2.  La zone de recherches, notifications, et de personnalisation du
    profil
3.  La colonne de gauche réservée aux échanges
4.  La colonne de droite pour les votes
5.  Et le bouton d’accès rapide aux principales actions

![02 framavox bienvenue commente](images/02-framavox-bienvenue-commente.png)

Bon, pour continuer, il lui faut du monde avec qui échanger. Qu’à cela
ne tienne, elle décide d’inviter John et Olivia à tester l’outil avec
elle pour le compte de l’association.
![03 framavox inviter des personnes](images/03-framavox-inviter-des-personnes.png)

Et, tant qu’à faire, autant créer la première discussion sur un sujet
clivant, brûlant, un sujet qui divise le Groupe d’Action pour le Gras :

![04 framavox creer discussion](images/04-framavox-créer-discussion.png)

L’outil invite aux échanges, John et Olivia s’en donnent à cœur joie,
comme on peut le lire dans la colonne des échanges de cette nouvelle
discussion.

![05 framavox discussion](images/05-framavox-discussion.png)

Prise d’un doute subit, Sandrine se demande si les objections de John sont
liées au fait que la margarine est une graisse végétale (lui qui adore
les graisses animales). Elle se demande surtout si d’autres membres du
GAG ne font pas partie d’une «&nbsp;majorité silencieuse&nbsp;» qui souhaiterait
que l’asso défende en priorité les graisses animales. Elle décide donc
de lancer un vote rapide, une «&nbsp;proposition&nbsp;», afin que l’on valide le
positionnement de l’asso avant que de poursuivre les échanges.

![06 framavox decision colonne droite](images/06-framavox-decision-colonne-droite.png)

Le résultat est unanime : personne dans l’association ne veut se
restreindre à la promotion exclusive des graisses animales. Cette
question (fondamentale, il faut bien le dire) étant réglée, les échanges
se poursuivent… jusqu’au point où il est temps de décider : le G.A.G. se
lance-t-il (ou non) dans cette campagne de promotion «&nbsp;Margarine, ma
passion&nbsp;»&nbsp;? Sandrine décide de peaufiner la proposition afin que
chacun-e comprenne l’interprétation des votes possibles. Elle utilise
donc la syntaxe markdown pour mettre son texte en page.

![08 framavox proposition 2 combine fleche](images/08-framavox-proposition-2-combine-fleche.png)

Bon&hellip; John a décider de poser son veto sur cette action, en votant «
Bloquer&nbsp;»… mais le vote n’étant pas clos, Olivia veut mieux comprendre
sa position, et se demande s’il a bien toutes les cartes en main. Elle
décide de le notifier en écrivant «&nbsp;@johnbutter&nbsp;» afin qu’il reçoive ce
message dans ses emails.

![09 framavox notification](images/09-framavox-notification.png)

John, qui n’aime pas la margarine parce que c’est une «&nbsp;copie du beurre&nbsp;» (il
préfère l’huile de coco, le bougre), n’avait pas pensé à ces arguments.
Il répond donc directement à son email de notification depuis sa
messagerie :

![10 framavox réponse email](images/10-framavox-réponse-email.png)

Et il se connecte à [Framavox](https://framavox.org) afin de changer son
vote de «&nbsp;Bloquer&nbsp;» à «&nbsp;S’abstient&nbsp;».

![11 framavox vote modifié](images/11-framavox-vote-modifié.png)

Sandrine est heureuse de voir en temps réel les échanges et les positions évoluer
sur cette discussion, qui reste ouverte encore quelques jours, le temps
que les autres membres du Groupe d’Action pour le Gras se connectent à
leur tour.

![12 framavox proposition 2 resultat](images/12-framavox-proposition-2-resultat.png)

Comme le G.A.G. est une association très active, il y a toujours plusieurs
sujets brûlant sur lesquels discuter, échanger et se positionner. Par
exemple, pendant tous ces échanges margariniers, il y a eu en parallèle
une autre discussion créée par Olivia, pour inventer une action autour
de ses recettes à l’huile d’olive.

![13 framavox nouvelle discussion](images/13-framavox-nouvelle-discussion.png)


## La morale de cette histoire…

C’est qu’on espère voir les recettes d’Olivia paraître sous licence
libre&nbsp;! (vous pouvez [rejoindre le groupe Framavox du
GAG](https://framavox.org/g/iGS1RDFC/le-groupe-d-action-pour-le-gras) si
vous voulez tester \^\^) C’est surtout que les équipes de
[Loomio](https://www.loomio.org/) ont conçu un outil qui peut faciliter
grandement la vie de collectifs faisant le choix de prendre des
décisions collaborativement. C’est enfin que
[Framavox](https://framavox.org/) est un nouvel outil à votre
disposition. Le tuto pour l’installer sur vos serveurs est d’ores et
déjà [disponible sur
framacloud.org](http://framacloud.org/cultiver-son-jardin/installation-de-loomio/).
*Et oui, vous avez le droit de vous demander à quoi on carbure quand on
cherche des exemples. C’est en vente libre, promis.*

### Pour aller plus loin :

-   [Les différents types de vote](types-votes.html), la suite de cet exemple
-   [Framavox](https://framavox.org)
-   [Documentation en français de Loomio](README.md)
-   Site officiel de [Loomio](https://www.loomio.org) \[anglais\]
-   [Dégooglisons Internet](https://degooglisons-internet.org), le site.

