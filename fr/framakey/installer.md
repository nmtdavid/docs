# Installer la Framakey

## Le choix de la clé USB, du disque dur externe…

Il est recommandé d’utiliser une « chaine USB 2.0 », 
c’est à dire une clé USB 2.0 (ou plus) sur un port (branchement) USB 2.0 (ou plus) 
Si l’un ou l’autre des éléments de cette chaine utilise une technologie USB 1.1, 
la vitesse de l’extraction peut être sensiblement plus lente 
(comptez entre 10mn et 3H pour décompresser les fichiers suivant 
la qualité de votre clé USB).

La Framakey fonctionne de façon optimale sur une chaine USB 2.0 mais 
toutes les clés USB 2.0 ne sont pas équivalentes. 
Des différences importantes peuvent être constatées quant à 
la rapidité d’exécution des applications de la Framakey.

La mémoire vive disponible est également un élément déterminant pour 
la rapidité d’exécution des applications de la Framakey. 


## Installation

Aprés avoir téléchargé le pack (`FramakeyInstaller_xxx.exe`) sur votre 
disque dur (ne pas le mettre directement sur la clé), lancez l’executable par un double-clic.

![](images/FK_2-0-2_Installation1.png)

Cliquer sur « Oui ».

![](images/FK_2-0-2_Installation2.png)

Lisez la licence. Cochez « J’accepte les termes de la licence » 
et cliquez sur suivant.

![](images/FK_2-0-2_Installation3.png)

Renseignez la lettre de lecteur assignée à votre clé USB (exemple: `F:\`) 
ou cliquez sur « Parcourir ».

![](images/FK_2-0-2_Installation4.png)

Sélectionnez la lettre correspondant à votre clé USB et cliquez sur « OK ».

![](images/FK_2-0-2_Installation5.png)

Cliquez sur « Installer ».

![](images/FK_2-0-2_Installation6.png)

L’installation démarre. Suivant les performances de votre clé USB, 
du fait que le port USB soit à la norme 1.1 ou 2.0, ou des performances 
générales de votre ordinateurs, le temps peut etre trés variable.

![](images/FK_2-0-2_Installation7.png)

Voila, votre Framakey est installée. Cliquez sur « Fermer ».

![](images/FK_2-0-2_Installation8.png)

Vous pouvez donc maintenant lancer l’interface de la Framakey 
en cliquant sur « oui ».