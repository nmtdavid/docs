# Framalistes

[Framalistes](https://framalistes.org)  vous permet de créer des listes de diffusion d'emails : toute personne s´abonnant à votre liste pourra recevoir les emails qui y sont envoyés, et y participer à son tour. À vous de choisir si cette liste est publique, semi-privée ou privée.

Le service repose sur le logiciel libre [Sympa](https://www.sympa.org/).

Pour mieux comprendre les fonctionalités et possibilités de ce service, voyez notre [exemple d'utilisation](exemple-d-utilisation.md).

---

## Tutoriel vidéo

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/1060l.jpg" height="340" width="570">
    <source src="https://framatube.org/blip/framalistes.mp4" type="video/mp4">
    <source src="https://framatube.org/blip/framalistes.webm" type="video/webm">
  </video></p>
  <p>→ La <a href="https://framatube.org/blip/framalistes.webm">vidéo au format webm</a></p>
</div>

Vidéo réalisée par Nicolas Geiger pour le site [collecti.cc/](http://www.collecti.cc/).

### Pour aller plus loin :

-   Tester [Framalistes](https://framalistes.org)
-   Découvrir [les listes
    publiques](https://framalistes.org/sympa/lists)
-   Le [site officiel de Sympa](https://www.sympa.org/) (à soutenir !)
-   [Installer
    Sympa](http://framacloud.org/cultiver-son-jardin/installation-de-sympa/)
    sur vos serveurs
-   [Dégooglisons Internet](http://degooglisons-internet.org/)
-   [Soutenir Framasoft](https://soutenir.framasoft.org)
