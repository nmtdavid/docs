<p class="alert alert-warning">La documentation est en cours de rédaction.<br>
Si vous souhaitez apporter des compléments, vous pouvez contribuer
sur <a href="https://framagit.org/framasoft/docs">le dépôt git</a>,
soit en ouvrant une « issue » décrivant les modifications,
soit en proposant une « merge request ».</p>

# Prise en main

## Gestion des images

### Images globales

Le dossier `/images` contient l’ensemble des images communes à tout le site.

Il sert de référence pour les modules et notament le module <code>images-collage</code>
ce qui permet d’éviter d’avoir à saisir l’ensemble du chemin d’accès aux images ;
le nom du fichier seul suffit.

Dans d’autres contextes il faudra utiliser la syntaxe markdown
<pre><code>![](/images/une-image.jpg)</code></pre>

L’adresse de l’image sera du genre :

    https://demo.frama.site/user/pages/images/une-image.jpg

### Images relatives

Pour les pages et articles, il est possible d’ajouter des images
directement liées au contenu de l’article.
Pour pouvoir uploader les fichiers, il faut que l’article ou la page
ait déjà été engegistré (sans nécéssairement le publier).

La première image ajoutée sert systématiqument d’image « à la une »
pour les articles de blog.

Pour ajouter une image dans le corps du texte, passez simplement la souris sur l’image et
cliquez sur le bouton <i class="fa fa-plus-circle"></i> à sa droite.
Un code markdown de ce genre devrait être inséré :
<pre><code>![](une-image.jpg)</code></pre>

Quand l’article sera publié l’adresse de l’image sera du genre :

    https://demo.frama.site/user/pages/01.page/article/une-image.jpg

## Utilisation des templates

### En cours…

## Personnalisation du site

### La barre de navigation et le pied de page
Excepté pour les `page_navbar_interne`, la barre de navigation et
le pied de page sont définis dans la page `common`.

<p class="alert alert-danger">Lorsque vous modifiez cette page, il est important de bien faire attention
que les shortcodes soient correctement fermés.<br>
S’il manque une balise de fermeture, le site ainsi que l’espace admin
riquent d’être complètement cassés.</p>

Le menu principal contient la liste des pages de premier niveau.

Le titre « Framasite » est défini dans l’attribut `brand_text`.
Les icônes de réseau sociaux en haut à droite correspondent aux shortcodes `[g-link]`
dont la syntaxe est décrite dans les [composants de base](/fr/grav/composants-de-base.html#ic%C3%B4nes-et-liens).

Voici une vidéo illustrant comment personnaliser tout ça :

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted poster="images/gravstrap-navbar-footer.jpg" class="embed-responsive-item">
      <source src="images/gravstrap-navbar-footer.mp4" type="video/mp4">
  </video>
</div>

### Ajouter des styles

Pour personnaliser l’apparence du site, il faut utiliser une feuille de style CSS.

Rendez-vous dans `Plugins > CustomCSS` et collez dans le champ `Inline CSS` votre feuille de style.

Illustration en vidéo avec un thème noir/orange dont
[la feuille de style est celle-ci](images/grav-dark.css).

<div class="embed-responsive embed-responsive-4by3">
  <video controls="controls" preload="none" muted poster="images/grav-customcss.jpg" class="embed-responsive-item">
      <source src="images/grav-customcss.mp4" type="video/mp4">
  </video>
</div>
