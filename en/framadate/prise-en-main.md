# How to quickly schedule a meeting with Framadate

<p class="h4">
    A free and open source service which respects privacy and personal datas,
    provided by <b class="violet">Frama</b><b class="orange">soft</b>
</p>

## 0. Opening

![](img/1.png)

To schedule an event, log on <https://framadate.org>.

<p>Then, click on
<button class="btn btn-primary">
    <i class="glyphicon glyphicon-calendar" aria-hidden="true"></i>
    Schedule an event
</button></p>

Please note that you can request a list of all your polls by clicking
on «&nbsp;Where are my polls&nbsp;».
You  will have to fill in the form with your e-mail adress.

---

## 1. Settings

![](img/2.png)

Fill in the presentation form.

VYour e-mail adress will only be used to communicate informations about
the survey (in accordance with
[the Framasoft's Services Charter](https://framasoft.org/nav/html/charte.en.html)).

**Checkboxes&nbsp;:** choose from the options available.
*Trick&nbsp;: receive an e-mail for each new vote will help you to
follow the advancement of the survey.*

![](img/3.png)

Set the dates by using the contextual menu, and refine the poll by
entering hours.
The buttons + and – allow you to add as many fields as you want.

<p class="alert alert-info">
    Note the button
    <button class="btn btn-default"
        title="Copy times from the first day">
        <i class="glyphicon glyphicon-sort-by-attributes-alt text-info" aria-hidden="true"></i>
        <span class="sr-only">Copy times from the first day</span>
    </button>
    which allows you to copy the hours of the first day to the other.
</p>

![](img/5.png)

A page presents the summary of your poll. Here, you can enter yourself
a date for automatic deletion.

Then, you may **validate** the creation of your poll.

---

## 2. Administration

![](img/6.png)

After the creation of your poll, you log directly into the
administration side.
Datas will be sent to you by e-mail containing the following URLs :

 *  **the public link of the poll** which should be sent to your
    colleagues or friends,
 *  **the admin link** which should be stored until the end of the survey.

If needed, you are able to fill in the poll by entering your own
preferences or the ones of another person.

Note : from this admin page, you can <strong>export</strong> the results
of the poll in a .csv format (which can be opened with a spreadsheet
application like Excel or LibreOffice Calc).

<p>Just click on the button
<button class="btn btn-default">
    <i class="glyphicon glyphicon-download-alt" aria-hidden="true"></i>
    Export to CSV
</button>
 to proceed.</p>

---

## 3. Vote in practice

![](img/7.png)

 1. Write your name.
 -  For each field, choose «&nbsp;yes&nbsp;»,
    «&nbsp;if needed&nbsp;» or «&nbsp;no&nbsp;» (by default).
 -  Save your choices.
 -  It is possible to visualize the results on a diagram.
 -  After each saved vote, the results of the poll are updated.

---

<p class="text-muted text-center">
    Author: Christophe Masutti, Framasoft, june 2015<br>
    This How To is under
    <a href="http://artlibre.org/licence/lal/">Art Libre License</a>.<br>
    <img src="img/Logo_Licence_Art_Libre.png" alt="" width="20%" />
</p>